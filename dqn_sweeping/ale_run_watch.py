""" This script runs a pre-trained network with the game
visualization turned on.

Usage:

ale_run_watch.py NETWORK_PKL_FILE [ ROM ]
"""
import subprocess
import sys

def run_watch():
    print sys.argv[1]
    command = ['./run_nature_sweep.py', '--glue-port', '4096', 
               '--steps-per-epoch', '0',
               '--test-length', '18000', '--nn-file', sys.argv[1],
               '-e', '30']

    if len(sys.argv) > 2:
	print sys.argv[2]
        command.extend(['--rom', sys.argv[2]])

    print ' '.join(command)
    p1 = subprocess.Popen(command, shell=True)
    
    p1.wait()

if __name__ == "__main__":
    run_watch()
