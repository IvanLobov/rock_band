import requests
from dateutil.parser import parse
import numpy as np

from requests.auth import HTTPBasicAuth
resp = requests.get("http://52.18.203.20/scores/team2/scores.txt", auth=('team2', 'E @k|YuzkQN2*6k'))

result = {'gopher': {}, 'seaquest': {}, 'tutankham': {}}
data = resp.text.split('\n')
for line in data:
	v = line.strip().split(';')
	if len(v) > 5:
		result[v[2]][parse(v[4]).timestamp()] = v[5]


def get_mean(r):
	return np.mean([int(t) for t in [r[k] for k in sorted(r, reverse=1)[0:29]]])

result['gopher'] = get_mean(result['gopher'])
result['seaquest'] = get_mean(result['seaquest'])
result['tutankham'] = get_mean(result['tutankham'])

print(result)
