# Clone cron backup scripts
cd ~/ & git clone https://github.com/gerrich/nn_saver
cd ~/setup
cat cron.txt > crontab -e

# Clone last version of DQN Torch code
git clone https://github.com/soumith/deepmind-atari.git

# Clone Ale scripts
git clone https://github.com/gerrich/ale_team_runner.git